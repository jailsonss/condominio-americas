<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'americas-web');

// Project repository
set('repository', 'git@bitbucket.org:suporte-qualitare/condominio-americas.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server 
set('writable_dirs', []);

// Keep only the latest two releases
set('keep_releases', 2);

// Hosts
host('condominiodasamericas.com.br')
	->port(2310)
	->user('root')
	->set('deploy_path', '/home/condominiodasame/{{application}}');

// Fix user permission
task('deploy:chown', function () {
	run('chown -R condominiodasame:condominiodasame ' . get('deploy_path'));
});

// Tasks
desc('Deploy your project');
task('deploy', [
	'deploy:info',
	'deploy:prepare',
	'deploy:lock',
	'deploy:release',
	'deploy:update_code',
	'deploy:shared',
	'deploy:writable',
	'deploy:chown',
	'deploy:clear_paths',
	'deploy:symlink',
	'deploy:unlock',
	'cleanup',
	'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
