import Vue from 'vue'
import Router from 'vue-router'
// import AMIntro from './components/pages/AMIntro'
import AMLanding from './components/pages/AMLanding'
import AMTimeline from './components/pages/AMTimeline'
import AMBlog from './components/pages/AMBlog'
import AMPost from './components/pages/AMPost'
import { TweenMax, Power2 } from 'gsap/TweenMax'

Vue.use(Router)
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
		{
  			path: '/',
  			redirect: 'home/'
  		},
	    {
	        path: '/home/',
	        name: 'home',
	        component: AMLanding
	    },
	    {
	        path: '/acompanhamento/',
	        name: 'acompanhamento',
	        component: AMTimeline
	    },
	    {
	        path: '/blog/',
	        name: 'blog',
	        component: AMBlog
	    },
	    {
	        path: '/post/:slug',
	        name: 'blog',
	        components: {
				post: AMPost
			}
	    },
	],
	scrollBehavior (to, from, savedPosition) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve({ x: 0, y: 0 })
            }, 400)
        })
    }
	// scrollBehavior(to, from, savedPosition) {
	// 	return new Promise((resolve, reject) => {
	// 		TweenMax.to(document.querySelector('.ch-app__container'), .5, {
	// 			scrollTop: 0,
	// 			ease: Power2.easeOut,
	// 			onComplete() {
	// 				return resolve({ x: 0, y: 0 })
	// 			}
	// 		})
	// 	})
	// }
})

router.afterEach((to, from) => {
	document.dispatchEvent(new Event('app.rendered'))
	/* window.scrollTo(0, 0);
	 */
	/* this.$router.go(); */
})

export default router
