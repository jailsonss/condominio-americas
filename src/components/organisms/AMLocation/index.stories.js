import { storiesOf } from '@storybook/vue'
import AMLocation from './index'

storiesOf('Organism - am-location', module)
	.add('default', () => ({
		components: {
			'am-location': AMLocation
		},
		data: () => ({
			//
		}),
		template: `
			<am-location />
		`
	}))
