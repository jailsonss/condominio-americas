import { storiesOf } from '@storybook/vue'
import AMSection from './index'

storiesOf('Organism - am-section', module)
	.add('default', () => ({
		components: { 'am-section': AMSection },
		template: `
			<am-section />
		`
	}))
