import { storiesOf } from '@storybook/vue'
import AMAlbum from './index'

storiesOf('Organism - am-album', module)
	.add('default', () => ({
		components: {
			'am-album': AMAlbum
		},
		data: () => ({
			//
		}),
		template: `
			<am-album :style="{ height: '640px' }" />
		`
	}))
