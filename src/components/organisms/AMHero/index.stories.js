import { storiesOf } from '@storybook/vue'
import AMHero from './index'

storiesOf('Organism - am-hero', module)
	.add('default', () => ({
		components: {
			'am-hero': AMHero
		},
		data: () => ({
			//
		}),
		template: `
			<am-hero />
		`
	}))
