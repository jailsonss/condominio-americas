import { storiesOf } from '@storybook/vue'
import AMTimeline from './index'

storiesOf('Organism - am-timeline', module)
	.add('default', () => ({
		components: {
			'am-timeline': AMTimeline
		},
		data: () => ({
			//
		}),
		template: `
			<am-timeline />
		`
	}))
