import { storiesOf } from '@storybook/vue'
import AMTeam from './index'

storiesOf('Organism - am-team', module)
	.add('default', () => ({
		components: { 'am-team': AMTeam },
		template: `
			<am-team />
		`
	}))
