import { storiesOf } from '@storybook/vue'
import AMBlog from './index'

storiesOf('Organism - am-blog', module)
	.add('default', () => ({
		components: {
			'am-blog': AMBlog
		},
		data: () => ({
			//
		}),
		template: `
			<am-blog />
		`
	}))
