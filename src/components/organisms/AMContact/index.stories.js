import { storiesOf } from '@storybook/vue'
import AMContact from './index'

storiesOf('Organism - am-contact', module)
	.add('default', () => ({
		components: {
			'am-contact': AMContact
		},
		data: () => ({
			//
		}),
		template: `
			<am-contact />
		`
	}))
