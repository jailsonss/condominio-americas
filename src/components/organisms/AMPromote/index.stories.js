import { storiesOf } from '@storybook/vue'
import AMPromote from './index'

storiesOf('Organism - am-promote', module)
	.add('default', () => ({
		components: {
			'am-promote': AMPromote
		},
		data: () => ({
			//
		}),
		template: `
			<am-promote />
		`
	}))
