import { storiesOf } from '@storybook/vue'
import AMList from './index'

storiesOf('Molecule - am-list', module)
	.add('default', () => ({
		components: { 'am-list': AMList },
		template: `
			<am-list />
		`
	}))
