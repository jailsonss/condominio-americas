import { storiesOf } from '@storybook/vue'
import AMToolbar from './index'

storiesOf('Molecule - am-toolbar', module)
	.add('default', () => ({
		components: {
			'am-toolbar': AMToolbar
		},
		data: () => ({
			active: false
		}),
		template: `
			<am-toolbar
				:active.sync="active"
			/>
		`
	}))
