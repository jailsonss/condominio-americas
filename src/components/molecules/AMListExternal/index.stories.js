import { storiesOf } from '@storybook/vue'
import AMList from './index'

storiesOf('Molecule - am-list-external', module)
	.add('default', () => ({
		components: { 'am-list-external': AMList },
		template: `
			<am-list-external />
		`
	}))
