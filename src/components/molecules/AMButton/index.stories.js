import { storiesOf } from '@storybook/vue'
import CHButton from './index'

storiesOf('Molecule - am-button', module)
	.add('default', () => ({
		data: () => ({
			container: {
				display: 'flex',
				flexFlow: 'row wrap',
				alignItems: 'center',
				justifyContent: 'space-around'
			}
		}),
		components: { 'ch-button': CHButton },
		template: `
		<div :style="container">
			<ch-button icon="book" size="sm" :filled="false">
				Clique aqui
			</ch-button>
			<ch-button icon="enter" size="md" :filled="false">
				Entrar no site
			</ch-button>
			<ch-button icon="play-o" size="lg" :filled="true">
				Assista ao Vídeo
			</ch-button>
			<ch-button icon="send" size="sm" :filled="true">
				Enviar
			</ch-button>
			<ch-button icon="check" size="lg" :filled="true">
				Enviado
			</ch-button>
		</div>
		`
	}))
