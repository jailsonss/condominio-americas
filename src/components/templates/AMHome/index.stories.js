import { storiesOf } from '@storybook/vue'
import AMHome from './index'

storiesOf('Template - am-home', module)
	.add('default', () => ({
		components: {
			'am-home': AMHome
		},
		data: () => ({
			view: {
				height: '800px'
			}
		}),
		template: `
			<am-home :style="view" />
		`
	}))
