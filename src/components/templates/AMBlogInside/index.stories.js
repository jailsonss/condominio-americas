import { storiesOf } from '@storybook/vue'
import AMTimelineInside from './index'

storiesOf('Template - am-timeline-inside', module)
	.add('default', () => ({
		components: {
			'am-timeline-inside': AMTimelineInside
		},
		data: () => ({
			view: {
				height: '800px'
			}
		}),
		template: `
			<am-timeline-inside :style="view" />
		`
	}))
