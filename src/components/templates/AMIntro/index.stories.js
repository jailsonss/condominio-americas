import { storiesOf } from '@storybook/vue'
import AMIntro from './index'

storiesOf('Organism - am-intro', module)
	.add('default', () => ({
		components: {
			'am-intro': AMIntro
		},
		data: () => ({
			view: {
				height: '800px'
			}
		}),
		template: `
			<am-intro :style="view" />
		`
	}))
