import { storiesOf } from '@storybook/vue'
import AMHamburger from './index'

storiesOf('Atom - am-hamburger', module)
	.add('primary', () => ({
		data: () => ({
			active: false
		}),
		components: { 'am-hamburger': AMHamburger },
		template: `
			<am-hamburger v-model="active" />
		`
	}))

