import { storiesOf } from '@storybook/vue'
import AMText from './index'

storiesOf('Atom - am-text', module)
	.add('body', () => ({
		components: { 'am-text': AMText },
		template: `
			<am-text type="body">
				Curabitur bibendum eget leo in posuere. Integer elementum purus faucibus diam placerat, sit amet accumsan eros commodo. Phasellus in suscipit quam, ut suscipit quam. Donec semper fringilla leo, ut scelerisque tellus finibus vitae.
			</am-text>
		`
	}))
	.add('subtitle', () => ({
		components: { 'am-text': AMText },
		template: `
			<am-text type="subtitle">
				Integer elementum purus faucibus diam placerat, sit amet accumsan eros commodo. Phasellus in suscipit quam, ut suscipit quam.
			</am-text>
		`
	}))
	.add('title', () => ({
		components: { 'am-text': AMText },
		template: `
			<am-text type="title">
				Phasellus in suscipit quam, ut suscipit quam.
			</am-text>
		`
	}))
	.add('caption', () => ({
		components: { 'am-text': AMText },
		template: `
			<am-text type="caption">
				Seu nome
			</am-text>
		`
	}))
	.add('placeholder', () => ({
		components: { 'am-text': AMText },
		template: `
			<am-text type="placeholder">
				Selecione um dos ítems
			</am-text>
		`
	}))
	.add('button', () => ({
		components: { 'am-text': AMText },
		template: `
			<am-text type="button">
				Inscrever-se
			</am-text>
		`
	}))
	.add('nav', () => ({
		components: { 'am-text': AMText },
		template: `
			<am-text type="nav">
				Home
			</am-text>
		`
	}))
