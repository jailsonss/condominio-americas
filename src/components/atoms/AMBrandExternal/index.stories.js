import { storiesOf } from '@storybook/vue'
import AMBrand from './index'

storiesOf('Atom - am-brand', module)
	.add('default', () => AMBrand)
	.add('typo', () => ({
		components: {
			'am-brand': AMBrand
		},
		template: `
			<am-brand :typo="true" />
		`
	}))

