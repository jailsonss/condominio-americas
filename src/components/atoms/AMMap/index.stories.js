import { storiesOf } from '@storybook/vue'
import AMMap from './index'

storiesOf('Atom - am-map', module)
	.add('primary', () => ({
		data: () => ({
			coords: { lat: -7.1137721, lng: -34.8332407 }
		}),
		components: { 'am-map': AMMap },
		template: `
			<am-map :center="coords" @click.native="coords = { lat: -7.6137721, lng: -34.8332407 }" />
		`
	}))
